import { configure, addParameters } from '@storybook/react';
import theme from './theme';

const req = require.context('../src', true, /\.stories.js$/);

function loadStories() {
  req.keys().forEach((filename) => req(filename));
}

addParameters({
  options: {
    theme
  }
});

configure(loadStories, module);
