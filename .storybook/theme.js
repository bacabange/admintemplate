import { create } from '@storybook/theming';

export default create({
  base: 'light',

  colorPrimary: '#f9216c',
  colorSecondary: '#4986ff',

  // UI
  appBg: '#ffffff',
  appContentBg: '#ffffff',
  appBorderColor: '#cccccc',
  appBorderRadius: 0,

  // Typography
  fontBase: '"Open Sans", sans-serif',
  fontCode: 'monospace',

  // Text colors
  textColor: 'black',
  textInverseColor: 'rgba(255,255,255,0.9)',

  // Toolbar default and active colors
  barTextColor: '#f9216c',
  barSelectedColor: 'black',
  // barBg: '#f9216c',

  // Form colors
  inputBg: 'white',
  inputBorder: 'silver',
  inputTextColor: 'black',
  inputBorderRadius: 4,

  brandTitle: 'La Manicurista',
  brandUrl: 'https://lamanicurista.com',
  brandImage: 'https://lamanicurista.com/wp-content/uploads/2019/01/Logo-La-manicurista-1.png'
});
