# La Manicurista Admin Console

## Installation

``` bash
# clone the repo
$ git clone git clone https://bacabange@bitbucket.org/bacabange/admintemplate.git AdminConsole

# go into app's directory
$ cd AdminConsole

# install app's dependencies
$ npm install
# or yarn
$ yarn
```

## Basic usage

``` bash
# dev server with hot reload at http://localhost:3000
$ npm start
# or yarn
$ yarn start
```

Navigate to [http://localhost:3000](http://localhost:3000). The app will automatically reload if you change any of the source files.

## Build

Run `build` to build the project. The build artifacts will be stored in the `build/` directory.

```bash
# build for production with minification
$ npm run build
# or yarn
$ yarn build
```
