export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer',
      badge: {
        variant: 'info',
        text: 'NEW'
      }
    },
    {
      title: true,
      name: 'Operación',
      wrapper: {
        // optional wrapper object
        element: '', // required valid HTML5 element tag
        attributes: {} // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: '' // optional class names space delimited list for title item ex: "text-center"
    },
    {
      name: 'Todos los Servicios',
      url: '/theme/colors',
      icon: 'icon-list'
    },
    {
      name: 'Servicios Inmediatos',
      url: '/theme/typography',
      icon: 'fa fa-bolt'
    },
    {
      name: 'Servicios Reservados',
      url: '/theme/typography',
      icon: 'fa fa-clock-o'
    },
    {
      name: 'Clientes',
      url: '/theme/typography',
      icon: 'fa fa-user'
    },
    {
      name: 'Profesionales',
      url: '/buttons',
      icon: 'fa fa-users',
      children: [
        {
          name: 'Habilidades',
          url: '/buttons/buttons',
          icon: 'fa fa-list-alt'
        },
        {
          name: 'Horarios',
          url: '/buttons/button-dropdowns',
          icon: 'fa fa-calendar'
        },
        {
          name: 'Profesionales',
          url: '/buttons/button-groups',
          icon: 'fa fa-users'
        },
        {
          name: 'Mapa',
          url: '/buttons/brand-buttons',
          icon: 'fa fa-street-view'
        }
      ]
    },
    {
      title: true,
      name: 'Administración',
      wrapper: {
        element: '',
        attributes: {}
      }
    },
    {
      name: 'Cuentas',
      url: '/base',
      icon: 'fa fa-money',
      children: [
        {
          name: 'Actualizar Cuentas',
          url: '/base/breadcrumbs',
          icon: 'fa fa-refresh'
        },
        {
          name: 'Liquidación',
          url: '/base/cards',
          icon: 'fa fa-calculator'
        },
        {
          name: 'Abonos',
          url: '/base/carousels',
          icon: 'fa fa-cloud-upload'
        },
        {
          name: 'Balance Diario',
          url: '/base/collapses',
          icon: 'fa fa-calendar-check-o'
        }
      ]
    },
    {
      name: 'Reportes',
      url: '/base',
      icon: 'fa fa-pie-chart',
      badge: {
        variant: 'info',
        text: 'NEW'
      },
      children: [
        {
          name: 'Clientes',
          url: '/base/breadcrumbs',
          icon: 'fa fa-file-o'
        },
        {
          name: 'Servicios',
          url: '/base/cards',
          icon: 'fa fa-file-o'
        },
        {
          name: 'Profesionales',
          url: '/base/carousels',
          icon: 'fa fa-file-o'
        },
        {
          name: 'Cuentas',
          url: '/base/collapses',
          icon: 'fa fa-file-o'
        }
      ]
    },
    {
      name: 'Desempeño',
      url: '/icons',
      icon: 'fa fa-line-chart',
      children: [
        {
          name: 'Parámetros',
          url: '/icons/coreui-icons',
          icon: 'fa fa-th-large',
          badge: {
            variant: 'info',
            text: 'NEW'
          }
        },
        {
          name: 'Reglas',
          url: '/icons/flags',
          icon: 'fa fa-th-list'
        }
      ]
    },
    {
      divider: true
    },
    {
      title: true,
      name: 'Sistema'
    },
    {
      name: 'Configuración',
      url: '/pages',
      icon: 'fa fa-cogs',
      children: [
        {
          name: 'Login',
          url: '/login',
          icon: 'icon-star'
        },
        {
          name: 'Register',
          url: '/register',
          icon: 'icon-star'
        },
        {
          name: 'Error 404',
          url: '/404',
          icon: 'icon-star'
        },
        {
          name: 'Error 500',
          url: '/500',
          icon: 'icon-star'
        }
      ]
    }
    /* {
      name: 'Disabled',
      url: '/dashboard',
      icon: 'icon-ban',
      attributes: { disabled: true }
    }
     {
      name: 'Download CoreUI',
      url: 'https://coreui.io/react/',
      icon: 'icon-cloud-download',
      class: 'mt-auto',
      variant: 'success',
      attributes: { target: '_blank', rel: "noopener" },
    },
    {
      name: 'Try CoreUI PRO',
      url: 'https://coreui.io/pro/react/',
      icon: 'icon-layers',
      variant: 'danger',
      attributes: { target: '_blank', rel: "noopener" },
    }, */
  ]
};
