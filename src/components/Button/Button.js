import React from 'react';
import PropTypes from 'prop-types';
import { Button as RButton } from 'reactstrap';

const Button = (props) => {
  const { children, ...restProps } = props;
  return <RButton {...restProps}>{children}</RButton>;
};

Button.propTypes = {
  outline: PropTypes.bool,
  active: PropTypes.bool,
  block: PropTypes.bool,
  color: PropTypes.string,
  disabled: PropTypes.bool,
  tag: PropTypes.instanceOf(React.ReactType),
  innerRef: PropTypes.instanceOf(React.Ref),

  onClick: PropTypes.func,
  size: PropTypes.any,
  id: PropTypes.string,
  style: PropTypes.instanceOf(React.CSSProperties)
};

Button.defaultProps = {
  outline: false,
  active: false,
  block: false,
  color: 'primary',
  disabled: false
};

export default Button;
