import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import '../../App.scss';
import '../../scss/storybook.scss';

import Button from './Button';

export function createButton(attrs) {
  return {
    id: Math.round(Math.random() * 1000000).toString(),
    color: 'default',
    ...attrs
  };
}

export const actions = {
  onClick: action('onClick')
};

const colors = ['primary', 'secondary', 'success', 'danger', 'info', 'warning', 'dark', 'link'];

storiesOf('Button', module)
  .addDecorator((story) => <div className="p-4">{story()}</div>)
  .add('Normal', () => (
    <>
      <div className="row">
        {colors.map((color) => (
          <div className="col" key={color}>
            <Button block {...createButton({ color })} {...actions}>
              {color}
            </Button>
            <Button active block {...createButton({ color })} {...actions}>
              {color}
            </Button>
            <Button disabled block {...createButton({ color })} {...actions}>
              {color}
            </Button>
          </div>
        ))}
      </div>
    </>
  ))
  .add('Outline', () => (
    <>
      <div className="row">
        {colors.map((color) => (
          <div className="col" key={color}>
            <Button outline block {...createButton({ color })} {...actions}>
              {color}
            </Button>
            <Button outline active block {...createButton({ color })} {...actions}>
              {color}
            </Button>
            <Button outline disabled block {...createButton({ color })} {...actions}>
              {color}
            </Button>
          </div>
        ))}
      </div>
    </>
  ))
  .add('Sizes', () => (
    <>
      <div className="row mb-2">
        <Button {...createButton({ color: 'primary', size: 'sm' })} {...actions}>
          Small
        </Button>
      </div>
      <div className="row mb-2">
        <Button {...createButton({ color: 'info' })} {...actions}>
          Normal
        </Button>
      </div>
      <div className="row mb-2">
        <Button {...createButton({ color: 'success', size: 'lg' })} {...actions}>
          Large
        </Button>
      </div>
    </>
  ));
