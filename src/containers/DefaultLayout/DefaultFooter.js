import React from 'react';

const propTypes = {};
const defaultProps = {};

const DefaultFooter = () => {
  return (
    <React.Fragment>
      <span className="ml-auto">
        <a href="https://lamanicurista.com">LaManicurista</a> &copy; 2019 - v2.0.0
      </span>
      {/* <span className="ml-auto">
          Powered by <a href="https://coreui.io/react">CoreUI for React</a>
        </span> */}
    </React.Fragment>
  );
};

DefaultFooter.propTypes = propTypes;
DefaultFooter.defaultProps = defaultProps;

export default DefaultFooter;
