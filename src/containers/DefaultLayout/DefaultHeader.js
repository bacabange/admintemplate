import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { Badge, UncontrolledDropdown, DropdownItem, DropdownMenu, DropdownToggle, Nav, NavItem } from 'reactstrap';
import PropTypes from 'prop-types';

import { AppAsideToggler, AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
import logo from '../../assets/img/brand/logo.svg';
import sygnet from '../../assets/img/brand/sygnet.svg';

const propTypes = {
  children: PropTypes.node
};

const defaultProps = {};

class DefaultHeader extends Component {
  render() {
    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          full={{ src: logo, width: 110, height: 40, alt: 'LaManicurista Logo' }}
          minimized={{ src: sygnet, width: 30, height: 30, alt: 'LaManicurista Logo' }}
        />
        <AppSidebarToggler className="d-md-down-none" display="lg" />

        {/* <Nav className="d-md-down-none" navbar>
          <NavItem className="px-3">
            <NavLink to="/dashboard" className="nav-link">
              Dashboard
            </NavLink>
          </NavItem>
        </Nav> */}
        <Nav className="ml-auto" navbar>
          {/* <NavItem className="d-md-down-none">
            <NavLink to="#" className="nav-link">
              <i className="icon-bell"></i>
              <Badge pill color="danger">
                5
              </Badge>
            </NavLink>
          </NavItem> */}

          <UncontrolledDropdown nav direction="down" className="d-md-down-none">
            <DropdownToggle nav>
              <i className="icon-globe"></i>
            </DropdownToggle>
            <DropdownMenu right>
              <DropdownItem header tag="div" className="text-center">
                <strong>Lenguaje</strong>
              </DropdownItem>
              <DropdownItem>
                <i className="flag-icon flag-icon-co"></i> ES - Español
              </DropdownItem>
              <DropdownItem>
                <i className="flag-icon flag-icon-us"></i> EN - English
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>

          <UncontrolledDropdown nav direction="down">
            <DropdownToggle nav>
              <i className="icon-bell"></i>
              <Badge pill color="danger">
                2
              </Badge>
            </DropdownToggle>
            <DropdownMenu right>
              <DropdownItem header tag="div" className="text-center">
                <strong>Notificaciones</strong>
              </DropdownItem>
              <DropdownItem>
                {/* TODO: Crear componente */}
                <div className="message">
                  <div>
                    <small className="text-muted">Cali</small>
                    <small className="text-muted float-right mt-1">A few seconds ago</small>
                  </div>
                  <div className="text-truncate font-weight-bold">
                    {/* <span className="fa fa-circle text-danger mr-1"></span> */}
                    Nuevo Servicio
                    <Badge className="mr-1" color="danger">
                      Inmediato
                    </Badge>
                  </div>
                  <div className="small text-muted text-truncate">
                    Servicio No. 144672 para 2 de Julio de 2019 a las 06:00 pm
                  </div>
                </div>
              </DropdownItem>

              <DropdownItem>
                <div className="message">
                  <div>
                    <small className="text-muted">Bogotá</small>
                    <small className="text-muted float-right mt-1">A few seconds ago</small>
                  </div>
                  <div className="text-truncate font-weight-bold">
                    {/* <span className="fa fa-circle text-danger mr-1"></span> */}
                    Nuevo Servicio
                    <Badge className="mr-1" color="secondary">
                      Reserva
                    </Badge>
                  </div>
                  <div className="small text-muted text-truncate">
                    Servicio No. 144672 para 2 de Julio de 2019 a las 06:00 pm
                  </div>
                </div>
              </DropdownItem>
              <DropdownItem onClick={e => this.props.onLogout(e)}>
                <i className="fa fa-check"></i> Marcar como leídos
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>

          <UncontrolledDropdown nav direction="down">
            <DropdownToggle nav>
              <img src={'../../assets/img/avatars/1.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com" />
            </DropdownToggle>
            <DropdownMenu right>
              <DropdownItem header tag="div" className="text-center">
                <strong>Stiven Castillo</strong>
              </DropdownItem>
              <DropdownItem>
                <i className="fa fa-user"></i> Perfil
              </DropdownItem>
              <DropdownItem>
                <i className="fa fa-wrench"></i> Configuración
              </DropdownItem>
              <DropdownItem onClick={e => this.props.onLogout(e)}>
                <i className="fa fa-lock"></i> Logout
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </Nav>
        <AppAsideToggler className="d-md-down-none" />
        {/*<AppAsideToggler className="d-lg-none" mobile />*/}
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default DefaultHeader;
