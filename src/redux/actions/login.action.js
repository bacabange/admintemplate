import LOGIN_USER_REQUEST from '../types';

const loginRequest = (email, password) => {
  return {
    type: LOGIN_USER_REQUEST,
    email,
    password
  };
};

export const fetchLogin = (urlApi, method, body = {}) => {
  return dispatch => {
    dispatch(
      loginRequest({
        email: body.email,
        password: body.password
      })
    );
  };
};

export default {
  fetchLogin
};
