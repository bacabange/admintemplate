import { combineReducers } from 'redux';
import { loginReducer } from './login.reducer';

const Reducers = combineReducers({
  loginReducer
});

export default Reducers;
