import LOGIN_USER_REQUEST from '../types';

const initialState = {
  email: '',
  password: '',
  data: null,
  status: 0,
  meta: null
};

function loginReducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN_USER_REQUEST:
      return Object.assign({}, state);
    default:
      return state;
  }
}

export { loginReducer };
