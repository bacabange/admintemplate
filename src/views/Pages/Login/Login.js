import React, { Component } from 'react';
import {
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row
} from 'reactstrap';
import logo from '../../../assets/img/brand/logo-white.png';
import actions from '../../../redux/actions/index';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

class Login extends Component {
  onLogin = () => {
    const { history } = this.props;
    console.log(history);
    history.push('/dashboard');
    // this.props.fetchLogin(null, 'post', { email: 'e@i.com', password: '123' });
  };

  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form>
                      <h1>Login</h1>
                      <p className="text-muted">Ingrese sus credenciales</p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" placeholder="Email" autoComplete="username" />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="password" placeholder="Contraseña" autoComplete="current-password" />
                      </InputGroup>
                      <Row>
                        <Col xs="5">
                          <Button color="primary" className="px-4" onClick={this.onLogin}>
                            Entrar
                          </Button>
                        </Col>
                        <Col xs="7" className="text-right">
                          <Button color="link" className="px-0">
                            Olvidé mi Contraseña
                          </Button>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
                <Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: '44%' }}>
                  <CardBody className="text-center">
                    <div>
                      <div className="row justify-content-md-center">
                        <div className="col-md-6">
                          <img src={logo} alt="La Manicurista" className="img-fluid" />
                          <p>Aplicación Móvil</p>
                        </div>
                      </div>
                    </div>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.loginReducer
  };
}

export default withRouter(
  connect(
    mapStateToProps,
    actions
  )(Login)
);
